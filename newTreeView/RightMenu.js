﻿            var menuskin = "skin0";       
            var display_url = false;  
            function getQuery(name)
            {
              var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
              var r = window.location.search.substr(1).match(reg);
              if (r!=null) return unescape(r[2]); return null;
            }  
            function hidemenuie5()
            {     
                ie5menu.style.visibility = "hidden";   
            }      
            function highlightie5(evt) 
            {      
                var event=evt || window.event;   
                var element=event.srcElement || event.target;   
                if (element.className == "menuitems")
                {   
                    element.style.backgroundColor = "highlight";   
                    element.style.color = "white";   
                    if (display_url)   
                    window.status = event.srcElement.url;   
               }   
            }     
            function lowlightie5(evt)
            {      
                var event=evt || window.event;   
                var element=event.srcElement || event.target;   
                if (element.className == "menuitems")
                {   
                    element.style.backgroundColor = "";   
                    element.style.color = "black";     
                }   
            }      
            function jumptoie5(evt)
            { 
                hidemenuie5();    
                var event=evt || window.event;   
                var element=event.srcElement || event.target; 
                var id = null;
                if (element.className == "menuitems") 
                    var id = element.getAttribute("id");  
                if(id != null)
                {
                    switch(id)
                    {
                        case "addNode":
                            addLeafNode();
                            break;
                        case "deleteNode":
                            deleteLeafNode();
                            break;
                        case "updateNode":
                            updateNodeText();
                            break;
                        case "showCurrentNodeHTML":
                            showNodeHTML();
                            break;  
                        case "expand":
                            expandCurrentNode();
                            break;  
                        case "collapse":
                            collapseCurrentNode();
                            break;         
                    }
                }  
            }        
            MzTreeView.prototype.showcontextmenu = function()
            {  
            
                var event=arguments[0] || window.event; 
                
                var rightedge = document.body.clientWidth-event.clientX;   
                var bottomedge = document.body.clientHeight-event.clientY;   
                  
                if (rightedge <ie5menu.offsetWidth)   
                    ie5menu.style.left = document.body.scrollLeft + event.clientX - ie5menu.offsetWidth;   
                else   
                    ie5menu.style.left = document.body.scrollLeft + event.clientX;   
                  
                if (bottomedge <ie5menu.offsetHeight)   
                    ie5menu.style.top = document.body.scrollTop + event.clientY - ie5menu.offsetHeight;   
                else   
                    ie5menu.style.top = document.body.scrollTop + event.clientY;   
                //右键点击节点出现 上下文关联菜单    
                //ie5menu.style.visibility = "visible";   
                //return false;   
            };
            document.onmousedown = hidemenuie5;